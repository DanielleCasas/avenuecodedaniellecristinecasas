This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Avenuecode test Danielle Cristine Casas

### How do I get set up? ###
* Summary of set up Clone repository 
* Configuration n/a
* Dependencies :Oracle java 1.8
* Database configuration n/a
* How to run tests run mvn test
* Deployment instructions run mvn spring-boot:run

DB have 4 products with id: 1 or 2 or 3 or 4
Product product1 = new Product();
Product product2 = new Product();
Product product3 = new Product();
Product product4 = new Product();
		
Image image1 = new Image();
Image image2 = new Image();
Image image3 = new Image();
		
image1.setType("jpg");
image2.setType("jpeg");
image3.setType("ong");
		
product1.setDescription("produto 1");
product2.setDescription("produto 2");
product3.setDescription("produto 3");
product4.setDescription("produto 4");
		
product1.setName("produto 1");
product2.setName("produto 2");
product3.setName("produto 3");
product4.setName("produto 4");
		
product2.setParentProduct(product1);
product3.setParentProduct(product1);
product4.setParentProduct(product1);
		
List<Image> images = new ArrayList<Image>();
images.add(image1);
images.add(image2);
images.add(image3);
product1.setImages(images);

add prdocut
/rest/product/save

update product
/rest/product/update

delete product
/rest/product/delete/{id}

add image
/rest/image/save

update image
/rest/image/update

delete image
/rest/image/delete/{id}



/rest/product to get all products including specified relationships(child product and images)
/rest/product/productswithrelated to get all products including specified relationships(child product)
/rest/product/productswithimages to get all products including specified relationships(child images)

/rest/product/prdwithrelated/1 to get product including specified relationships(child product) by product identity
/rest/product/prdwithimages/1 to get product including specified relationships(images) by product identity

/rest/product/imagesbyprod/1 to get set of images for specific product
/rest/product/getallrelatedbyid/1 to get set of child products for specific product

/rest/product/productswithoutrelated to get all products excluding relationships (child products, images)
/rest/product/prdwithoutrelations/1 to get all products excluding relationships (child products, images) using specific product identity

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

Test classes are not working fine, a had no time to implement this properly.