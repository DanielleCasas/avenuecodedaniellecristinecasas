package br.com.avenuecoderest.daniellecasas.model;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Danielle Casas
 *
 */

@Entity
@Table(name = "product")
public class Product implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "description", length = 50)
	private String description;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="IMG_PROD_REF")
	private Collection<Image> images = new LinkedHashSet<Image>();
	
	@ManyToOne(optional = true, fetch= FetchType.EAGER)
    @JoinColumn(name="PARENT_PROD_ID")
    private Product parentProduct;
	
	@Transient
	private List<Product> relatedProduts;

	public Product()
	{}

	public Product(Long id, String name, String description, Collection<Image> images, Product parentProduct) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.images = images;
		this.parentProduct = parentProduct;

	}
	
	public Product(Long id, String name, String description, Collection<Image> images) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.images = images;

	}
	
	public Product(Long id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;

	}

	public Product(Long id, String name, String description,
			Collection<Image> images, Product parentProduct,
			List<Product> relatedProduts) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.images = images;
		this.parentProduct = parentProduct;
		this.relatedProduts = relatedProduts;
	}

	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	public List<Product> getRelatedProduts() {
		return relatedProduts;
	}

	public void setRelatedProduts(List<Product> relatedProduts) {
		this.relatedProduts = relatedProduts;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<Image> getImages() {
		return images;
	}

	public void setImages(Collection<Image> images) {
		this.images = images;
	}

}
