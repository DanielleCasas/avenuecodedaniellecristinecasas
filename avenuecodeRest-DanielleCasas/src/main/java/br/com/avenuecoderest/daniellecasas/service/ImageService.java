package br.com.avenuecoderest.daniellecasas.service;

import br.com.avenuecoderest.daniellecasas.model.Image;
/**
 * 
 * @author Danielle Casas
 *
 */
public interface ImageService {
	
	void save(Image image);
	
	void update(Image image);
	
	void delete(Long id);
	
}
