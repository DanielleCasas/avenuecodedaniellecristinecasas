package br.com.avenuecoderest.daniellecasas.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;
import br.com.avenuecoderest.daniellecasas.service.ProductService;

/**
 * 
 * @author Danielle Casas
 *
 */

@RestController
@RequestMapping("/product")
public class ProductController {
	
	final static Logger logger = Logger.getLogger(ProductController.class);

	@Autowired
	ProductService productService;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<Product> createUser(@RequestBody Product product, UriComponentsBuilder ucBuilder) {
 
        if (product == null) {
            return new ResponseEntity<Product>(HttpStatus.CONFLICT);
        }
 
        productService.save(product);

       
        return new ResponseEntity<Product>(product, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<Product> updateUser(@RequestBody Product product) {
 
		productService.update(product);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Product> deleteUser(@PathVariable("id") long id) {
 
        productService.delete(id);
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<Product> getAllProducts() {
		return productService.getAllProducts();
	}
	
	@RequestMapping(value = "/prdwithrelated/{id}", method = RequestMethod.GET)
	public @ResponseBody Product getProductWithRelatedProductsById(@PathVariable("id") Long id) {
		return productService.getProductWithAllRelatedProductsById(id);
	}
	
	@RequestMapping(value = "/imagesbyprod/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Image> getAllImagesByProductId(@PathVariable("id") Long id) {
		return productService.getAllImagesByProductId(id);
	}
	
	
	@RequestMapping(value = "/prdwithimages/{id}", method = RequestMethod.GET)
	public @ResponseBody Product getProductWithImagesById(@PathVariable("id") Long id) throws JsonProcessingException {
		return productService.getProductWithAllImagesById(id);
	}

	
	@RequestMapping(value = "/productswithrelated", method = RequestMethod.GET)
	public @ResponseBody List<Product> getAllProductsWithRelatedProducts() {
		return productService.getAllProductsWithRelatedProducts();
	}
	
	@RequestMapping(value = "/productswithoutrelated", method = RequestMethod.GET)
	public @ResponseBody List<Product> getAllProductsWithoutRelations() {
		return productService.getAllProductsWithoutRelations();
	}
	
	@RequestMapping(value = "/prdwithoutrelations/{id}", method = RequestMethod.GET)
	public @ResponseBody Product getProductWithoutRelationsById(@PathVariable("id") Long id) throws JsonProcessingException {
		return productService.getProductWithoutRelationsById(id);
	}

	@RequestMapping(value = "/productswithimages", method = RequestMethod.GET)
	public @ResponseBody List<Product> getAllWithImages() {
		return productService.getAllProductsWithImages();
	}
	
	@RequestMapping(value = "/getallrelatedbyid/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Product> getAllRelatedProductsByProductId(@PathVariable("id") Long id) throws JsonProcessingException {
		return productService.getAllRelatedProductsByProductId(id);
	}

}
