package br.com.avenuecoderest.daniellecasas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectWriter.GeneratorSettings;

import br.com.avenuecoderest.daniellecasas.DAO.ProductDAO;
import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;

/**
 * 
 * @author Danielle Casas
 *
 */
@Service
public class DefaultProductService implements ProductService {

	@Autowired
	private ProductDAO productDAO;

	@Override
	public List<Product> getAllProducts() {
		return productDAO.getAllProducts();
	}

	@Override
	public Product getProductWithAllRelatedProductsById(Long id) {
		Product product = this.getProductWithoutRelationsById(id);
		product.setRelatedProduts(this.getAllRelatedProductsByProductId(id));
		return product;
	}

	@Override
	public List<Image> getAllImagesByProductId(Long id) {
		return productDAO.getAllImagesByProductId(id);
	}

	@Override
	public Product getProductWithAllImagesById(Long id) {
		Product prd = getProductWithoutRelationsById(id);
			prd.setImages(this.getAllImagesByProductId(id));		
		return prd;
	}

	@Override
	public List<Product> getAllProductsWithRelatedProducts() {
		List<Product> prds = getAllProductsWithoutRelations();

		for (Product product : prds) {
			product.setRelatedProduts(this.getAllRelatedProductsByProductId(product.getId()));
		}
		return prds;
	}

	@Override
	public List<Product> getAllProductsWithImages() {
		
		List<Product> prds = getAllProductsWithoutRelations();

		for (Product product : prds) {
			product.setImages(this.getAllImagesByProductId(product.getId()));
		}
		return prds;
		
	}

	@Override
	public List<Product> getAllRelatedProductsByProductId(Long id) {
		return productDAO.getAllRelatedProductsById(id);
	}

	@Override
	public void save(Product product) {
		productDAO.save(product);

	}

	@Override
	public List<Product> getAllProductsWithoutRelations() {

		return productDAO.getAllProductsWithoutRelations();
	}

	@Override
	public Product getProductWithoutRelationsById(Long id) {
		return productDAO.getProductWithoutRelationsById(id);
	}

	@Override
	public void update(Product product) {
		productDAO.update(product);
		
	}

	@Override
	public void delete(Long id) {
		productDAO.delete(id);
		
	}

}
