package br.com.avenuecoderest.daniellecasas.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;

@Transactional
@Repository("productDAO")
public class DefaultProductDAO extends AbstractDAO implements ProductDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getAllProducts() {
        Query query = getSession().createQuery("from Product");
        return (List<Product>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getAllProductsWithImages() {
		Query query = getSession().createQuery("SELECT p.id, p.name, p.description, p.images FROM Product p ");
        return (List<Product>) query.list();
	}
	
	@Override
	public List<Image> getAllImagesByProductId(Long id) {
		Criteria criteria = getSession().createCriteria(Product.class);
		criteria.add(Restrictions.eq("id", id));
		Product product = (Product) criteria.uniqueResult();
        return (List<Image>) product.getImages();
	}

	@Override 
	public Product getProductWithAllImagesById(Long id) {
		Query query = getSession().createQuery("SELECT p.id, p.name, p.description, p.images FROM Product p WHERE p.id = :id ").setParameter("id", id);
        return (Product) query.uniqueResult();
	}


	@Override
	public List<Product>  getAllRelatedProductsById(Long id) {
		Query query = getSession().createQuery("FROM Product p WHERE p.parentProduct.id = :id").setParameter("id",id);
        return (List<Product>) query.list();
	}

	@Override
	public void save(Product product) {
		getSession().save(product);
	}

	@Override
	public List<Product> getAllProductsWithoutRelations() {
		Query query = getSession().createQuery("SELECT new br.com.avenuecoderest.daniellecasas.model.Product(p.id, p.name, p.description) FROM Product p ");
        return (List<Product>) query.list();
	}

	@Override
	public Product getProductWithoutRelationsById(Long id) {
		Query query = getSession().createQuery("SELECT new br.com.avenuecoderest.daniellecasas.model.Product(p.id, p.name, p.description) FROM Product p WHERE p.id = :id").setParameter("id",id);
        return (Product) query.uniqueResult();
	}

	@Override
	public void update(Product product) {
		getSession().saveOrUpdate(product);
		
	}

	@Override
	public void delete(Long id) {
		Product product = (Product) getSession().get(Product.class, id);
		getSession().delete(product);
		
	}
}
