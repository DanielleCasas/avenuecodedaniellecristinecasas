package br.com.avenuecoderest.daniellecasas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.avenuecoderest.daniellecasas.DAO.ImageDAO;
import br.com.avenuecoderest.daniellecasas.model.Image;

/**
 * 
 * @author Danielle Casas
 *
 */
@Service
public class DefaultImageService implements ImageService {

	@Autowired
	private ImageDAO imageDAO;

	@Override
	public void save(Image image) {
		imageDAO.save(image);

	}
	@Override
	public void update(Image image) {
		imageDAO.update(image);
		
	}

	@Override
	public void delete(Long id) {
		imageDAO.delete(id);
		
	}

}
