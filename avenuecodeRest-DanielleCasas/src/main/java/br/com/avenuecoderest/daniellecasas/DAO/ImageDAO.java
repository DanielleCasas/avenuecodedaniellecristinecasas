package br.com.avenuecoderest.daniellecasas.DAO;
import java.util.List;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;

public interface ImageDAO {
	
	void save(Image image);
	
	void update(Image image);
	
	void delete(Long id);
}
