package br.com.avenuecoderest.daniellecasas.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.service.ImageService;


/**
 * 
 * @author Danielle Casas
 *
 */

@RestController
@RequestMapping("/image")
public class ImageController {
	
	final static Logger logger = Logger.getLogger(ImageController.class);

	@Autowired
	ImageService imageService;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<Image> createUser(@RequestBody Image image) {
 
        if (image == null) {
            return new ResponseEntity<Image>(HttpStatus.CONFLICT);
        }
 
        imageService.save(image);
       
        return new ResponseEntity<Image>(image, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<Image> updateUser(@RequestBody Image image) {
 
		imageService.update(image);
        return new ResponseEntity<Image>(image, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Image> deleteUser(@PathVariable("id") long id) {
 
        imageService.delete(id);
        return new ResponseEntity<Image>(HttpStatus.NO_CONTENT);
    }
}
