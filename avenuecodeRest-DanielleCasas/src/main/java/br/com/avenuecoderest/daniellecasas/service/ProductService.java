package br.com.avenuecoderest.daniellecasas.service;

import java.util.List;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;
/**
 * 
 * @author Danielle Casas
 *
 */
public interface ProductService {
	
	List<Product> getAllProducts();
	List<Product> getAllProductsWithImages();
	List<Product> getAllProductsWithoutRelations();
	
	List<Image> getAllImagesByProductId(Long id);
	Product getProductWithAllImagesById(Long id);
	
	List<Product> getAllProductsWithRelatedProducts();
	List<Product> getAllRelatedProductsByProductId(Long id);
	
	Product getProductWithAllRelatedProductsById(Long id);	
	Product getProductWithoutRelationsById(Long id);
	
	void save(Product product);
	
	void update(Product product);
	
	void delete(Long id);
	
}
