package br.com.avenuecoderest.daniellecasas.DAO;
import java.util.List;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;

public interface ProductDAO {

	List<Product> getAllProducts();
	List<Product> getAllProductsWithImages();
	
	List<Image> getAllImagesByProductId(Long id);
	Product getProductWithAllImagesById(Long id);
	
	List<Product> getAllRelatedProductsById(Long id);
	List<Product> getAllProductsWithoutRelations();
	
	Product getProductWithoutRelationsById(Long id);
	
	void save(Product product);
	
	void update(Product product);
	
	void delete(Long id);
}
