package br.com.avenuecoderest.daniellecasas.DAO;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.avenuecoderest.daniellecasas.model.Image;

@Transactional
@Repository("imageDAO")
public class DefaultImageDAO extends AbstractDAO implements ImageDAO{

	@Override
	public void save(Image image) {
		getSession().save(image);
	}

	@Override
	public void update(Image image) {
		getSession().saveOrUpdate(image);
		
	}

	@Override
	public void delete(Long id) {
		Image image = (Image) getSession().get(Image.class, id);
		getSession().delete(image);
		
	}
}
