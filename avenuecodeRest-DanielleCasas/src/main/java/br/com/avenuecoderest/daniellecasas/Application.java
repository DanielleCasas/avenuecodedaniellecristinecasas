package br.com.avenuecoderest.daniellecasas;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;
import br.com.avenuecoderest.daniellecasas.service.DefaultProductService;

/**
 * 
 * @author Danielle Casas
 *
 */

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
				
		ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
	    DefaultProductService productService = ctx.getBean(DefaultProductService.class);
	    
		Product product1 = new Product();
		Product product2 = new Product();
		Product product3 = new Product();
		Product product4 = new Product();
		
		Image image1 = new Image();
		Image image2 = new Image();
		Image image3 = new Image();
		
		image1.setType("jpg");
		image2.setType("jpeg");
		image3.setType("ong");
		
		product1.setDescription("produto 1");
		product2.setDescription("produto 2");
		product3.setDescription("produto 3");
		product4.setDescription("produto 4");
		
		product1.setName("produto 1");
		product2.setName("produto 2");
		product3.setName("produto 3");
		product4.setName("produto 4");
		
		product2.setParentProduct(product1);
		product3.setParentProduct(product1);
		product4.setParentProduct(product1);
		
		List<Image> images = new ArrayList<Image>();
		images.add(image1);
		images.add(image2);
		images.add(image3);
		product1.setImages(images);
		
		productService.save(product1);
		productService.save(product2);
		productService.save(product3);
		productService.save(product4);
	}
}
