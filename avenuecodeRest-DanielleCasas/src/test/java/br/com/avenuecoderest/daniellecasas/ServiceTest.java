package br.com.avenuecoderest.daniellecasas;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.avenuecoderest.daniellecasas.model.Image;
import br.com.avenuecoderest.daniellecasas.model.Product;
import br.com.avenuecoderest.daniellecasas.service.DefaultProductService;


public class ServiceTest {
	
	@Autowired
	private DefaultProductService productService;
	
	@Test
	public void getAllProducts()
	{
		List<Product> products = productService.getAllProducts();		
		Assert.assertNotNull(products);
	}
	@Test
	public void getAllProductsWithImages()
	{
		List<Product> products = productService.getAllProductsWithImages();		
		Assert.assertNotNull(products);
	}
	@Test
	public void getAllProductsWithoutRelations()
	{
		List<Product> products = productService.getAllProductsWithoutRelations();		
		Assert.assertNotNull(products);
	}
	@Test
	public void getAllImagesByProductId()
	{
		List<Image> images = productService.getAllImagesByProductId(new Long(1));		
		Assert.assertNotNull(images);
	}
	@Test
	public void getProductWithAllImagesById()
	{
		Product products = productService.getProductWithAllImagesById(new Long(1));		
		Assert.assertNotNull(products);
	}
	@Test
	public void getAllProductsWithRelatedProducts()
	{
		List<Product> products = productService.getAllProductsWithRelatedProducts();		
		Assert.assertNotNull(products);
	}
	@Test
	public void getProductWithoutRelationsById()
	{
		Product product = productService.getProductWithoutRelationsById(new Long(1));		
		Assert.assertNotNull(product);
	}
	@Test
	public void getProductWithAllRelatedProductsById()
	{
		Product products = productService.getProductWithAllRelatedProductsById(new Long(1));		
		Assert.assertNotNull(products);
	}
	
	@Test
	public void getAllRelatedProductsByProductId()
	{
		List<Product> products = productService.getAllRelatedProductsByProductId(new Long(1));		
		Assert.assertNotNull(products);
	}
}
